import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Button, TouchableOpacity, Alert } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import {Appbar, TextInput} from 'react-native-paper';
import AsyncStorage from '@react-native-community/async-storage';
const EditAddress = ({navigation,route}) => {
    //const { itemId } = route.params 
    const { itemId } = route.params
    const { itemPincode } = route.params
    const {itemCity}   = route.params
    const {itemFn}    =  route.params
    const {itemLn}    = route.params
    const {itemAddress}   = route.params
    const {itemLandmark}    = route.params
    const {itemMobile}    = route.params

  
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [Pincode, setPincode] = useState('');
  const [city, setCity] = useState('');
  const [Address, setAddress] = useState('');
  const [Landmark, setLandmark] = useState('');
  const [mobile, setMobile] = useState('');

  
    
  
    //   setFirstname(itemFn),
    //   setLastname(itemLn),
    //   setPincode(itemPincode),
    //   setCity(itemCity),
    //   setAddress(itemAddress),
    //   setLandmark(itemLandmark),
    //   setMobile(itemMobile)
  

//    useEffect( async() =>{
//     try {
//         let user = await AsyncStorage.getItem('userdetails');
//         user = JSON.parse(user);
//         setUserid(user.id)
//         //console.log('user id=>', user.id)
//         //console.log('userData =>', user);
//       } catch (error) {
//         console.log(error);
//       }
//   });


  const myfun = async() => {
    let user = await AsyncStorage.getItem('userdetails');
    user = JSON.parse(user);
    //let user_id= setUserid(user.id);
    console.log('user id=>', user.id)
    //Alert.alert(petname);
    //let user_id = {setUserid}
    let dataFetchUrl = 'http://3.12.158.241/medical/api/storeAddress/'+user.id;
    console.log('Addressurl---->', dataFetchUrl);
    await fetch(dataFetchUrl,{
        method : 'POST',
        headers:{
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "pincode": Pincode,
          "city_state": city,
          "address": Address,
          "landmark": Landmark,
          "first_name":firstname,
          "last_name" :lastname,
          "mobile": mobile,
                      
     })
    }).then(res => res.json())
    .then(resData => {
       console.log(resData);
       //Alert.alert(resData.message);
       if(resData.msg === 'User Details Created'){
        alert('Address Added Successfully')
        navigation.navigate('deliveryAddressStack')
       }
    });

    
 }

 console.log('pincode--->',itemId);
  

return(
    
  <ScrollView style={styles.contentBody}>
  
  <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
      <TextInput
        label= {itemPincode}
          value={Pincode}
          onChangeText={(itemPincode) => setPincode(itemPincode)}
          style={[styles.textInputall, styles.widtthirty]}
          />
      <TextInput
          label={itemCity}
          value={city}
          onChangeText={(e) => setCity(e.target.value)}
          style={[styles.textInputall, styles.widtsuxtysix]}
          />
  </View>
  <Text/>
  <Text/>
  <TextInput
      label={itemFn}
      value={firstname}
      onChangeText={(e) => setFirstname(e.target.value)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label={itemLn}
      value={lastname}
      onChangeText={(e) => setLastname(e.target.value)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label={itemAddress}
      value={Address}
      onChangeText={(e) => setAddress(e.target.value)}
      multiline = {true}
      numberOfLines = {5}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label={itemLandmark}
      value={Landmark}
      onChangeText={(e) => setLandmark(e.target.value)}
      style={styles.textInputall}
      />
  
  <Text/>
  <TextInput
      label={itemMobile}
      value={mobile}
      onChangeText={(e) => setMobile(e.target.value)}
      style={styles.textInputall}
      />
  
  <Text/>
  <View style={{alignItems: 'center'}}>
  <TouchableOpacity onPress={myfun} style={styles.appButtonContainer}>
         <Text style={styles.appButtonText}>Update</Text>
      </TouchableOpacity>
  </View>
  <Text/>
  </ScrollView>
);
}
export default EditAddress;

const styles = StyleSheet.create({
  image: {
      width: '100%'
  },
  contentBody: {
      paddingHorizontal: 15,
      paddingTop:10,
  },
  textInputall: {
      backgroundColor: '#fff'
  },
  widtthirty: {
      width: '30%',
  },
  widtfortyet: {
      width: '48%',
  },
  radios: {
      flexDirection: 'row',
      justifyContent: 'space-evenly'
  },
  widtsuxtysix: {
      width: '66%',
  },
  appButtonContainer: {
    elevation: 8,
    backgroundColor: "#1e90ff",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12,
    marginTop: 60,
    width: '50%',
    marginLeft: 10,
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
});