// Example of Splash, Login and Sign Up in React Native
// https://aboutreact.com/react-native-login-and-signup/

// Import React
import React,{Component, useState} from 'react';
import {Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';

// Import Navigators from React Navigation
import {NavigationContainer} from '@react-navigation/native';
//import {createStackNavigator} from '@react-navigation/stack';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';

// Import Screens
import HomeScreen from './DrawerScreens/HomeScreen';
import UP from './DrawerScreens/uploadPrescription';
import AddAdressScreen from './DrawerScreens/AddAdressScreen';
import SettingsScreen from './DrawerScreens/SettingScreen';
import myPrescriptions from './DrawerScreens/myPrescriptions';
import DeliveryAddress from './DeliveryAddress';
import EditAddress from './DrawerScreens/EditAddress'

import CustomSidebarMenu from './Components/CustomSidebarMenu';
import NavigationDrawerHeader from './Components/NavigationDrawerHeader';
import NavigationBack from './Components/NavigationBack';
import AsyncStorage from '@react-native-community/async-storage';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

// const AddressManageStack = () => {
//   return (
//     <Stack.Navigator>
//       <Stack.Screen name="AddAdressScreen" component={AddAdressScreen} />
//       <Stack.Screen name="DeliveryAddress" component={DeliveryAddress} />
//       <Stack.Screen name="EditAddress" component={EditAddress} />
//     </Stack.Navigator>
//   );
// };

const homeScreenStack = ({navigation}) => {
  //const [name,setName] = React.useState('');
  return (
    <Stack.Navigator initialRouteName="HomeScreen">
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{
          title: 'Home', //Set Header Title
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: '#307ecc', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
};

const UPStack = ({navigation}) => {
  return (
    <Stack.Navigator initialRouteName="UP">
      <Stack.Screen
        name="UP"
        component={UP}
        options={{
          title: 'Upload Prescription', //Set Header Title
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: '#307ecc', //Set Header color
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
          },
        }}
      />
    </Stack.Navigator>
  );
};

const settingScreenStack = ({navigation}) => {
  return (
    <Stack.Navigator
      initialRouteName="SettingsScreen"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerStyle: {
          backgroundColor: '#307ecc', //Set Header color
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
        },
      }}>
      <Stack.Screen
        name="SettingsScreen"
        component={SettingsScreen}
        options={{
          title: 'Personal Details', //Set Header Title
        }}
      />
      <Stack.Screen
        name="AddAdressScreen"
        component={AddAdressScreen}
        options={{
          headerLeft: () => (
            <TouchableOpacity onPress={()=>navigation.navigate('DeliveryAddress')}>
               <Icon name="chevron-left" size={25} color='#fff' style={{marginLeft:5,}}/>
            </TouchableOpacity>
          ),
          title: 'Add Address', //Set Header Title
        }}
      />
      <Stack.Screen
        name="DeliveryAddress"
        component={DeliveryAddress}
        options={{
          headerLeft: () => (
            <TouchableOpacity onPress={()=>navigation.navigate('SettingsScreen')}>
               <Icon name="chevron-left" size={25} color='#fff' style={{marginLeft:5,}}/>
            </TouchableOpacity>
          ),
          title: 'Delivery Address', //Set Header Title
        }}
      />
      <Stack.Screen
        name="EditAddress"
        component={EditAddress}
        options={{
          title: 'Edit Address', //Set Header Title
          headerLeft: () => (
            <TouchableOpacity onPress={()=>navigation.navigate('DeliveryAddress')}>
               <Icon name="chevron-left" size={25} color='#fff' style={{marginLeft:5,}}/>
            </TouchableOpacity>
          ),
        }}
      />
    </Stack.Navigator>
  );
};

// const deliveryAddressStack = ({navigation}) => {
//   return (
//     <Stack.Navigator
//       initialRouteName="DeliveryAddress"
//       screenOptions={{
//         headerLeft: () => (
//           <NavigationDrawerHeader navigationProps={navigation} />
//         ),
//         headerStyle: {
//           backgroundColor: '#307ecc', //Set Header color
//         },
//         headerTintColor: '#fff', //Set Header text color
//         headerTitleStyle: {
//           fontWeight: 'bold', //Set Header text style
//         },
//       }}>
//       <Stack.Screen
//         name="DeliveryAddress"
//         component={DeliveryAddress}
//         options={{
//           title: 'Delivery Address', //Set Header Title
//         }}
//       />
//     </Stack.Navigator>
//   );
// };

//  const editAddressStack = ({navigation}) => {
//    return (
//     <Stack.Navigator
//       initialRouteName="EditAddress"
//       screenOptions={{
//         headerLeft: () => (
//           <NavigationDrawerHeader navigationProps={navigation} />
//         ),
//         headerStyle: {
//           backgroundColor: '#307ecc', //Set Header color
//         },
//         headerTintColor: '#fff', //Set Header text color
//         headerTitleStyle: {
//           fontWeight: 'bold', //Set Header text style
//         },
//       }}>
//       <Stack.Screen
//         name="EditAddress"
//         component={EditAddress}
//         options={{
//           title: 'Edit Address', //Set Header Title
//         }}
//       />
//     </Stack.Navigator>
//   );
// };


// const AddressStack = ({navigation}) => {
//   return (
//     <Stack.Navigator
//       initialRouteName="AddAdressScreen"
//       screenOptions={{
//         headerLeft: () => (
//           <NavigationDrawerHeader navigationProps={navigation} />
//         ),
//         headerStyle: {
//           backgroundColor: '#307ecc', //Set Header color
//         },
//         headerTintColor:'#fff', //Set Header text color
//         headerTitleStyle: {
//           fontWeight: 'bold', //Set Header text style
//         },
//       }}>
//       <Stack.Screen
//         name="AddAdressScreen"
//         component={AddAdressScreen}
//         options={{
//           title: 'Add Address', //Set Header Title
//         }}
//       />
//     </Stack.Navigator>
//   );
// };

const PrescriptionStack = ({navigation}) => {
  return (
    <Stack.Navigator
      initialRouteName="myPrescriptions"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerStyle: {
          backgroundColor: '#307ecc', //Set Header color
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
        },
      }}>
      <Stack.Screen
        name="myPrescriptions"
        component={myPrescriptions}
        options={{
          title: 'My Prescription', //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
};

const DrawerNavigatorRoutes = (props) => {
  return (
    <Drawer.Navigator
      drawerContentOptions={{
        activeTintColor: '#cee1f2',
        color: '#cee1f2',
        itemStyle: {marginVertical: 5, color: 'white'},
        labelStyle: {
          color: '#d8d8d8',
        },
      }}
      screenOptions={{headerShown: false}}
      drawerContent={CustomSidebarMenu}>
      <Drawer.Screen
        name="homeScreenStack"
        options={{drawerLabel: 'Home Screen'}}
        component={homeScreenStack}
      />
      <Drawer.Screen
        name="settingScreenStack"
        options={{drawerLabel: 'Personal Details'}}
        component={settingScreenStack}
      />
      <Drawer.Screen
        name="UPStack"
        options={{drawerLabel: 'Upload Prescription'}}
        component={UPStack}
      />
      {/* <Drawer.Screen
        name="AddressStack"
        options={{drawerLabel: 'Add Address'}}
        component={AddressStack}
      /> */}
      <Drawer.Screen
        name="PrescriptionStack"
        options={{drawerLabel: 'My Prescription'}}
        component={PrescriptionStack} 
      />
      {/* <Drawer.Screen
        name="deliveryAddressStack"
        options={{drawerLabel: 'Delivery Address'}}
        component={deliveryAddressStack}
      /> */}
      
      </Drawer.Navigator>
    
  );
};

export default DrawerNavigatorRoutes;
